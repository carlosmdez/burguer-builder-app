import { AUTH_START, AUTH_SUCCESS, AUTH_FAIL, AUTH_LOGOUT, SET_AUTH_REDIRECT_PATH } from "../actions/actionTypes";

const initialState = {
  token: null,
  userId: null,
  error: null,
  loading: false,
  authRedirectPath: '/'
}

const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {

  case AUTH_START:
    return { ...state, error: null, loading: true }

  case AUTH_SUCCESS:
    return { ...state, token: payload.idToken, userId: payload.localId, error: false, loading: false }

  case AUTH_FAIL:
    return { ...state, error: payload, loading: false }

  case AUTH_LOGOUT:
    return { ...state, token: null, userId: null }

  case SET_AUTH_REDIRECT_PATH:
    return { ...state, authRedirectPath: payload}

  default:
    return state
  }
}

export default authReducer