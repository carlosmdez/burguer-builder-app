import React from 'react'
import authReducer from './authReducer'
import { AUTH_SUCCESS } from '../actions/actionTypes'

describe('Auth Reducer', () => {
  it('should return the initial state', () => {
    expect(authReducer(undefined, {})).toEqual({
      token: null,
      userId: null,
      error: null,
      loading: false,
      authRedirectPath: '/'
    })
  })

  it('should store token', () => {
    expect(
      authReducer(
        {
          token: null,
          userId: null,
          error: null,
          loading: false,
          authRedirectPath: '/'
        },
        {
          type: AUTH_SUCCESS,
          payload: { idToken: 'some-token', localId: 'some-id' }
        }
      )
    ).toEqual({
      token: 'some-token',
      userId: 'some-id',
      error: false,
      loading: false,
      authRedirectPath: '/'
    })
  })
})
