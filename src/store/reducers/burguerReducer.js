import { ADD_INGREDIENT, REMOVE_INGREDIENT, SET_INGREDIENTS, FETCH_INGREDIENTS_FAIL } from "../actions/actionTypes";


const INGREDIENT_PRICES = {
  salad: 3,
  bacon: 6,
  cheese: 5,
  meat: 20
}

const initialState = {
  ingredients: null,
  totalPrice: 25,
  error: false,
  building: false
}

const reducerBurguer = (state = initialState, { type, payload }) => {
  switch (type) {

    case ADD_INGREDIENT:
      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          [payload]: state.ingredients[payload] + 1
        },
        totalPrice: state.totalPrice + INGREDIENT_PRICES[payload],
        building: true
      }

    case REMOVE_INGREDIENT:
      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          [payload]: state.ingredients[payload] - 1
        },
        totalPrice: state.totalPrice - INGREDIENT_PRICES[payload],
        building: true
      }

    case SET_INGREDIENTS:
      return {
        ...state,
        ingredients: {
          salad: payload.salad,
          bacon: payload.bacon,
          cheese: payload.cheese,
          meat: payload.meat
        },
        totalPrice: 25,
        error: false,
        building: false
      }

    case FETCH_INGREDIENTS_FAIL:
      return {
        ...state,
        error: true
      }

    default:
      return state
  }
}

export default reducerBurguer
