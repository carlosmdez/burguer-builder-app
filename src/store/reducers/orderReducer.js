import {
  PURCHASE_BURGUER_SUCCESS,
  PURCHASE_BURGUER_FAIL,
  PURCHASE_BURGUER_START,
  PURCHASE_INIT,
  FETCH_ORDERS_START,
  FETCH_ORDERS_SUCCESS,
  FETCH_ORDERS_FAIL
} from '../actions/actionTypes'

const initialState = {
  orders: [],
  loading: true,
  purchased: false
}

const orderReducer = (state = initialState, { type, payload }) => {
  switch (type) {

    case PURCHASE_INIT:
      return { 
        ...state,
        purchased: false
      }

    case PURCHASE_BURGUER_START:
      return { 
        ...state,
        loading: true
      }

    case PURCHASE_BURGUER_SUCCESS:
      return { 
        ...state, 
        loading: false,
        purchased: true,
        // orders: state.orders.concat(payload)
      }

    case PURCHASE_BURGUER_FAIL:
      return { ...state, loading: false }

    case FETCH_ORDERS_START:
      return { ...state, loading: true }

    case FETCH_ORDERS_SUCCESS:
      return { ...state, orders: payload, loading: false }

    case FETCH_ORDERS_FAIL:
      return { ...state, loading: false }


    default:
      return state
  }
}

export default orderReducer
