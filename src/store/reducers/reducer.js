import { combineReducers } from 'redux'
import reducerBurguer from './burguerReducer';
import orderReducer from './orderReducer';
import authReducer from './authReducer'

export default combineReducers({
  reducerBurguer, orderReducer, authReducer
})