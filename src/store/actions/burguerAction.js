import axios from '../../axios-orders'

import {
  REMOVE_INGREDIENT,
  ADD_INGREDIENT,
  SET_INGREDIENTS,
  FETCH_INGREDIENTS_FAIL
} from './actionTypes'

export const addIngredient = value => {
  return {
    type: ADD_INGREDIENT,
    payload: value
  }
}

export const removeIngredient = value => {
  return {
    type: REMOVE_INGREDIENT,
    payload: value
  }
}

export const setIngredients = value => {
  return {
    type: SET_INGREDIENTS,
    payload: value
  }
}

export const fetchIngredientsFail = () => {
  return {
    type: FETCH_INGREDIENTS_FAIL
  }
}

export const initIngredients = () => {
  return dispatch => {
    axios
      .get('/ingredients.json')
      .then(response => {
        dispatch(setIngredients(response.data))
      })
      .catch(error => {
        dispatch(fetchIngredientsFail())
      })
  }
}
