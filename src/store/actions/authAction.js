import axios from 'axios'

import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_FAIL,
  AUTH_LOGOUT,
  SET_AUTH_REDIRECT_PATH
} from './actionTypes'

//==========================
//========== Auth ==========
//==========================

export const authStart = () => {
  return {
    type: AUTH_START
  }
}

export const authSuccess = value => {
  return {
    type: AUTH_SUCCESS,
    payload: value
  }
}

export const authFail = error => {
  return {
    type: AUTH_FAIL,
    payload: error
  }
}

export const authLogout = () => {
  localStorage.removeItem('token')
  localStorage.removeItem('userId')
  localStorage.removeItem('expirationDate')
  return {
    type: AUTH_LOGOUT
  }
}

export const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(authLogout())
    }, expirationTime * 1000)
  }
}

export const auth = payload => {
  return dispatch => {
    dispatch(authStart())
    const authData = {
      email: payload.email,
      password: payload.password,
      returnSecureToken: true
    }

    let url =
      'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyA2D3hKrvE1oy9R3WdyLp2KMtUWfkvuhrA'
    if (!payload.isSignUp) {
      url =
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyA2D3hKrvE1oy9R3WdyLp2KMtUWfkvuhrA'
    }
    axios
      .post(url, authData)
      .then(response => {
        const expirationDate = new Date(
          new Date().getTime() + response.data.expiresIn * 1000
        )
        localStorage.setItem('token', response.data.idToken)
        localStorage.setItem('userId', response.data.localId)
        localStorage.setItem('expirationDate', expirationDate)
        dispatch(authSuccess(response.data))
        dispatch(checkAuthTimeout(response.data.expiresIn))
      })
      .catch(error => {
        dispatch(authFail(error.response.data.error.message))
      })
  }
}

export const setAuthRedirectPath = path => {
  return {
    type: SET_AUTH_REDIRECT_PATH,
    payload: path
  }
}

export const authCheckState = () => {
  return dispatch => {
    const token = localStorage.getItem('token')
    if (!token) {
      dispatch(authLogout())
    } else {
      const expDate = new Date(localStorage.getItem('expirationDate'))
      if (expDate > new Date() ) {
        const userId = localStorage.getItem('userId')
        dispatch(authSuccess({idToken: token, localId: userId}))
        dispatch(checkAuthTimeout((expDate.getTime() - new Date().getTime()) / 1000 ))
      } else {
        dispatch(authLogout())
      }
    }
  }
}
