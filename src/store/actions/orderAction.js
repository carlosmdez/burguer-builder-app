import axios from '../../axios-orders'

import {
  PURCHASE_BURGUER_SUCCESS,
  PURCHASE_BURGUER_FAIL,
  PURCHASE_BURGUER_START,
  PURCHASE_INIT,
  FETCH_ORDERS_START,
  FETCH_ORDERS_SUCCESS,
  FETCH_ORDERS_FAIL
} from './actionTypes'

//==============================
//====== Purchase Burguer ======
//==============================

export const purchaseBurguerSuccess = value => {
  return {
    type: PURCHASE_BURGUER_SUCCESS,
    payload: value
  }
}

export const purchaseBurguerFail = error => {
  return {
    type: PURCHASE_BURGUER_FAIL,
    payload: error
  }
}

export const purchaseBurguerStart = () => {
  return {
    type: PURCHASE_BURGUER_START
  }
}

export const purchaseInit = () => {
  return {
    type: PURCHASE_INIT
  }
}

export const purchaseBurguer = (orderData, token) => {
  return dispatch => {
    dispatch(purchaseBurguerStart())
    axios
      .post(`/orders.json?auth=${token}`, orderData)
      .then(response => {
        dispatch(purchaseBurguerSuccess({ id: response.data.name, orderData }))
      })
      .catch(error => {
        dispatch(purchaseBurguerFail(error))
      })
  }
}

//==========================
//====== Fetch Orders ======
//==========================

export const fetchOrdersSuccess = value => {
  return {
    type: FETCH_ORDERS_SUCCESS,
    payload: value
  }
}

export const fetchOrdersFail = error => {
  return {
    type: FETCH_ORDERS_FAIL,
    payload: error
  }
}

export const fetchOrdersStart = () => {
  return {
    type: FETCH_ORDERS_START
  }
}

export const fetchOrders = (token, userId) => {
  return dispatch => {
    dispatch(fetchOrdersStart())
    const queryParams = `?auth=${token}&orderBy="userId"&equalTo="${userId}"`
    axios.get(`/orders.json${queryParams}`)
      .then(response => {
        dispatch(fetchOrdersSuccess(response.data))
      })
      .catch(error => {
        dispatch(fetchOrdersFail(error))
      })
  }
}
