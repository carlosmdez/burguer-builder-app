import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://react-burguershop.firebaseio.com/'
})

export default instance