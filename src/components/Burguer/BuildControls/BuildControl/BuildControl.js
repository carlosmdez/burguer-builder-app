import React from 'react'
import css from './BuildControl.css'

const BuildControl = (props) => {
  return (
    <div className={css.BuildControl}>
      <div className={css.Label}>{props.label}</div>
      <button className={css.Less} onClick={props.removeButton} disabled={props.disabled}>Less</button>
      <button className={css.More} onClick={props.addButton}>More</button>
    </div>
  )
}

export default BuildControl
