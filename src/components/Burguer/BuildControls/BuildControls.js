import React from 'react'

import css from './BuildControls.css'
import BuildControl from './BuildControl/BuildControl'

const controls = [
  { label: 'Salad', type: 'salad' },
  { label: 'Bacon', type: 'bacon' },
  { label: 'Cheese', type: 'cheese' },
  { label: 'Meat', type: 'meat' }
]

const BuildControls = props => {
  const transformedControls = controls.map(el => (
    <BuildControl
      key={el.label}
      label={el.label}
      addButton={() => props.ingredientAdded(el.type)}
      removeButton={() => props.ingredientRemoved(el.type)}
      disabled={props.disabled[el.type]}
    />
  ))

  return (
    <div className={css.BuildControls}>
      <p>
        Current price: <strong>{props.price.toFixed(2)} MXN</strong>
      </p>
      {transformedControls}
      <button
        className={css.OrderButton}
        onClick={props.clickOrder}
        disabled={!props.purchaseable}
      >
        {props.isAuth ? 'Order Now' : 'Sign Up to order'}
      </button>
    </div>
  )
}

export default BuildControls
