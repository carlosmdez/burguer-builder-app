import React from 'react'
import Button from '../../UI/Button/Button';

const OrderSumarry = props => {
  const ingredientsSumarry = Object.keys(props.ingredients).map(ing => {
    if (props.ingredients[ing] > 0) {
      return (
        <li key={ing}>
          <span style={{ textTransform: 'capitalize' }}>{ing}</span>:{' '}
          {props.ingredients[ing]}
        </li>
      )
    } else return null
  })
  return (
    <>
      <h3>Your order</h3>
      <p>Your delicious burguer contain the next ingredients: </p>
      <ul>{ingredientsSumarry}</ul>
      <p><strong>Total Price: {props.price.toFixed(2)}</strong></p>
      <p>Continue to Checkout?</p>
      <Button buttonType='Danger' clicked={props.purchaseCanceled}>Cancel</Button>
      <Button buttonType='Success' clicked={props.purchaseContinued}>Continue</Button>
    </>
  )
}

export default OrderSumarry
