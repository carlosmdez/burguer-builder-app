import React from 'react'

import css from './Burguer.css'
import BurguerIngredient from './BurguerIngredient/BurguerIngredient';

const Burguer = (props) => {
  let transformedIngredients = Object.keys(props.ingredients)
    .map(ing => {
      return [...Array(props.ingredients[ing])].map((_, i) => {
        return <BurguerIngredient key={ing + i} type={ing}/>
      })
    })
    .reduce((arr, el) =>{
      return arr.concat(el)
    }, [])
    if (transformedIngredients.length === 0) {
      transformedIngredients = <p> Please, start adding ingredients </p> 
    }
  return (
    <div className={css.Burguer}>
      <BurguerIngredient type='breadTop'/>
      {transformedIngredients}
      <BurguerIngredient type='breadBottom'/>
    </div>
  )
}

export default Burguer
