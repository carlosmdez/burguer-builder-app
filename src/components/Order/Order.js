import React from 'react'
import css from './Order.css'

const Order = props => {
  let { bacon, cheese, salad, meat } = props.ingredients
  return (
    <div className={css.Order}>
      <p>
        Ingredients: <span>Salad ({salad})</span> <span>Bacon ({bacon})</span>{' '}
        <span>Cheese ({cheese})</span> <span>Meat ({meat})</span>
      </p>
      <p>
        Price: <strong>{props.price.toFixed(2)} MXN</strong>
      </p>
    </div>
  )
}

export default Order
