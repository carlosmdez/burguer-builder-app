import React from 'react'
import css from './CheckoutSumarry.css'

import Burguer from '../../Burguer/Burguer';
import Button from '../../UI/Button/Button'

const CheckoutSumarry = (props) => {
  return (
    <div className={css.CheckoutSumarry}>
      <h1>Enjoy your delicious burguer!</h1>
      <div className={css.BurguerContent}>
      <Burguer ingredients={props.ingredients}/>
      </div>
      <Button buttonType='Danger' clicked={props.chechoutCancelled}>Cancel</Button>
      <Button buttonType='Success' clicked={props.chechoutContinued}>Continue</Button>
    </div>
  )
}

export default CheckoutSumarry
