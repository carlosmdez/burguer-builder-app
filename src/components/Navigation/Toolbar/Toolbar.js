import React from 'react'

import css from './Toolbar.css'
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';

const Toolbar = (props) => {
  return (
    <header className={css.Toolbar}>
      <DrawerToggle clicked={props.sideDrawerToggle}/>
      <Logo height={'80%'}/>
      <nav className={css.DesktopOnly}>
      <NavigationItems isAuth={props.isAuth}/>
      </nav>
    </header>
  )
}

export default Toolbar
