import React from 'react'
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import css from './SideDrawer.css'
import Backdrop from '../../UI/Backdrop/Backdrop'

const SideDrawer = (props) => {
  let attachedClasses = [css.SideDrawer, css.Close]
  if (props.open) {
    attachedClasses = [css.SideDrawer, css.Open]
  }
  return (
    <>
    <Backdrop show={props.open} modalClosed={props.closed}/>
    <div className={attachedClasses.join(' ')} onClick={props.closed}>
      <Logo height={'11%'} className={css.Logo}/>
      <nav>
        <NavigationItems isAuth={props.isAuth}/>
      </nav>
    </div>
    </>
  )
}

export default SideDrawer
