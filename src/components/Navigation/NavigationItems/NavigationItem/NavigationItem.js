import React from 'react'
import css from './NavigationItem.css'
import { NavLink } from 'react-router-dom'

const NavigationItem = (props) => {
  return (
    <li className={css.NavigationItem}>
      <NavLink to={props.link} exact={props.exact} activeClassName={css.active}>
        {props.children}
      </NavLink>
    </li>
  )
}

export default NavigationItem
