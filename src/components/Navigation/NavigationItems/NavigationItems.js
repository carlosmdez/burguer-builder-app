import React from 'react'

import css from './NavigationItems.css'
import NavigationItem from './NavigationItem/NavigationItem'

const NavigationItems = props => {
  let navAuth = <NavigationItem link="/auth">Authenticate</NavigationItem>
  if (props.isAuth) {
    navAuth = (
      <>
        <NavigationItem link="/orders">Orders</NavigationItem>
        <NavigationItem link="/logout">Logout</NavigationItem>
      </>
    )
  }
  return (
    <ul className={css.NavigationItems}>
      <NavigationItem link="/" exact>
        Burger Builder
      </NavigationItem>
      {navAuth}
    </ul>
  )
}

export default NavigationItems
