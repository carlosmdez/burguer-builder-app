import React from 'react'

import css from './Button.css'

const Button = props => {
  return (
    <button
      className={[css.Button, css[props.buttonType]].join(' ')}
      onClick={props.clicked}
      disabled={props.disabled}
    >
      {props.children}
    </button>
  )
}

export default Button
