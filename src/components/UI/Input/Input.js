import React from 'react'

import css from './Input.css'

const Input = props => {
  let inputElement = null
  let errorMessage = null
  const { elementType, elementConfig, value, invalid, label, shouldValidate, touched } = props
  const inputClasses = [css.InputElement] 
  if (invalid && shouldValidate && touched) {
    inputClasses.push(css.Invalid)
    errorMessage = <p className={css.ValidationError}>Please enter a valid value</p>
  }
  switch (elementType) {
    case 'input':
      inputElement = (
        <input
          className={inputClasses.join(' ')}
          {...elementConfig}
          value={value}
          onChange={props.changed}
        />
      )
      break
    case 'textarea':
      inputElement = (
        <textarea
          className={inputClasses.join(' ')}
          {...elementConfig}
          value={value}
          onChange={props.changed}
        />
      )
      break
    case 'select':
      inputElement = (
        <select
          className={inputClasses.join(' ')}
          value={value}
          onChange={props.changed}
        >
          {elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      )
      break
    default:
      inputElement = (
        <input
          className={inputClasses.join(' ')}
          {...elementConfig}
          value={value}
          onChange={props.changed}
        />
      )
      break
  }
  return (
    <div className={css.Input}>
      <label className={css.Label}>{label}</label>
      {inputElement}
      {errorMessage}
    </div>
  )
}

export default Input
