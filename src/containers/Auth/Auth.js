import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom';

import Input from '../../components/UI/Input/Input'
import Button from '../../components/UI/Button/Button'
import css from './Auth.css'
import { auth, setAuthRedirectPath } from '../../store/actions/actions'
import Spinner from '../../components/UI/Spinner/Spinner'

class Auth extends Component {
  state = {
    controls: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your e-mail'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Your password'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
        touched: false
      }
    },
    isSignUp: true
  }

  componentDidMount() {
    if (!this.props.building && this.props.authRedirectPath !== '/') {
      this.props.onSetAuthRedirectPath('/')
    }
  }

  checkValidity = (value, rules) => {
    let isValid = true
    if (rules) {
      if (rules.required) {
        isValid = value.trim() !== '' && isValid
      }

      if (rules.minLength) {
        isValid = value.length >= rules.minLength && isValid
      }

      if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid
      }
    }
    return isValid
  }

  inputChangedHandler = (event, controlName) => {
    const updatedControls = {
      ...this.state.controls,
      [controlName]: {
        ...this.state.controls[controlName],
        value: event.target.value,
        valid: this.checkValidity(
          event.target.value,
          this.state.controls[controlName].validation
        ),
        touched: true
      }
    }
    this.setState({ controls: updatedControls })
  }

  submitHandler = event => {
    event.preventDefault()
    this.props.onAuth(
      this.state.controls.email.value,
      this.state.controls.password.value,
      this.state.isSignUp
    )
  }

  switchAuthModeHandler = () => {
    this.setState(prevState => {
      return {isSignUp: !prevState.isSignUp}
    })
  }

  render() {
    const formElementsArray = Object.entries(this.state.controls)
    let form = formElementsArray.map(formElement => (
      <Input
        key={formElement[0]}
        elementType={formElement[1].elementType}
        elementConfig={formElement[1].elementConfig}
        value={formElement[1].value}
        invalid={!formElement[1].valid}
        shouldValidate={formElement[1].validation}
        touched={formElement[1].touched}
        changed={event => this.inputChangedHandler(event, formElement[0])}
      />
    ))
    if (this.props.loading) {
      form = <Spinner/>
    }

    let errorMessage = null
    if (this.props.error) {
      errorMessage = <p>{this.props.error}</p>
    }

    let authRedirect = null
    if (this.props.isAuthenticated) {
      authRedirect = <Redirect to={this.props.authRedirectPath}/>
    }

    return (
      <div className={css.Auth}>
      {authRedirect}
      {errorMessage}
        <form onSubmit={this.submitHandler}>
          {form}
          <Button buttonType="Success">Submit</Button>
        </form>
        <Button buttonType="Danger" clicked={this.switchAuthModeHandler}>Switch to {this.state.isSignUp ? 'SignIn' : 'SignUp'}</Button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loading: state.authReducer.loading,
  error: state.authReducer.error,
  isAuthenticated: state.authReducer.token !== null,
  building: state.reducerBurguer.building,
  authRedirectPath: state.authReducer.authRedirectPath
})

const mapDispatchToProps = dispatch => ({
  onAuth: (email, password, isSignUp) => dispatch(auth({ email, password, isSignUp })),
  onSetAuthRedirectPath: (path) => dispatch(setAuthRedirectPath(path))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth)
