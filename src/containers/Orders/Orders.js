import React, { Component } from 'react'
import { connect } from 'react-redux'

import Order from '../../components/Order/Order'
import axios from '../../axios-orders'
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../components/withErrorHandler/withErrorHandler';
import { fetchOrders } from '../../store/actions/actions';

class Orders extends Component {
  componentDidMount() {
    this.props.onFetchOrders(this.props.token, this.props.userId)
  }

  ordersMethod = () => {
    return Object.entries(this.props.orders).map( order => {
      return (
        <Order
        key={order[0]}
        ingredients={order[1].ingredients}
        price={order[1].price}/>
      )
    })
  }

  render() {
    let ordersArray = Object.entries(this.props.orders)
    let orders = (
      ordersArray.map( order => {
        return (
          <Order
          key={order[0]}
          ingredients={order[1].ingredients}
          price={order[1].price}/>
        )
      })
    )
    if (this.props.loading) {
      orders = <Spinner/>
    }
    return (
      <div>
        {orders}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  orders: state.orderReducer.orders,
  loading: state.orderReducer.loading,
  token:state.authReducer.token,
  userId:state.authReducer.userId
})

const mapDispatchToProps = dispatch => ({
  onFetchOrders: (token, userId) => dispatch(fetchOrders(token, userId))
})


export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, axios))