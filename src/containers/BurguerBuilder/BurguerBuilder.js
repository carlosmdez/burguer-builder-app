import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from '../../axios-orders'
import Burguer from '../../components/Burguer/Burguer'
import BuildControls from '../../components/Burguer/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSumarry from '../../components/Burguer/OrderSumarry/OrderSumarry'
import Spinner from '../../components/UI/Spinner/Spinner'
import withErrorHandler from '../../components/withErrorHandler/withErrorHandler'
import { addIngredient, removeIngredient, initIngredients, purchaseInit, setAuthRedirectPath } from '../../store/actions/actions'

export class BurguerBuilder extends Component {
  state = {
    purchaseButton: false
  }

  componentDidMount() {
    this.props.onInitIngredients()
  }

  updatePurchaseState = ingredients => {
    const sum = Object.keys(ingredients)
      .map(ing => {
        return ingredients[ing]
      })
      .reduce((lastValue, currentValue) => {
        return lastValue + currentValue
      }, 0)
    return sum > 0
  }

  purchaseHandler = () => {
    if (this.props.isAuthenticated) {
      this.setState({ purchaseButton: true })
    } else {
      this.props.onSetAuthRedirectPath('/checkout')
      this.props.history.push('/auth')
    }
  }

  purchaseCancelHandler = () => {
    this.setState({ purchaseButton: false })
  }

  purchaseContinueHandler = () => {
    this.props.onInitPurchase()
    this.props.history.push('/checkout')
  }

  render() {
    const disabledInfo = { ...this.props.ingBruguer}
    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0
    }

    let orderSumarry = null
    let burguer = this.props.error ? <p>Ingredients can't be showed</p> : <Spinner />
    if (this.props.ingBruguer) {
      burguer = (
        <>
          <Burguer ingredients={this.props.ingBruguer} />
          <BuildControls
            ingredientAdded={this.props.onAddIngredient}
            ingredientRemoved={this.props.onRemoveIngredient}
            disabled={disabledInfo}
            price={this.props.priceBurguer}
            purchaseable={this.updatePurchaseState(this.props.ingBruguer)}
            clickOrder={this.purchaseHandler}
            isAuth={this.props.isAuthenticated}
            />
        </>
      )
      orderSumarry = (
        <OrderSumarry
        ingredients={this.props.ingBruguer}
        purchaseCanceled={this.purchaseCancelHandler}
        purchaseContinued={this.purchaseContinueHandler}
        price={this.props.priceBurguer}
        />
        )
      }
      return (
      <>
        <Modal
          show={this.state.purchaseButton}
          modalClosed={this.purchaseCancelHandler}
        >
          {orderSumarry}
        </Modal>
        {burguer}
      </>
    )
  }
}

const mapStateToProps = state => ({
  ingBruguer: state.reducerBurguer.ingredients,
  priceBurguer: state.reducerBurguer.totalPrice,
  error: state.reducerBurguer.error,
  isAuthenticated: state.authReducer.token !== null
})

const mapDispatchToProps = dispatch => ({
  onAddIngredient: (ingredient) => dispatch(addIngredient(ingredient)),
  onRemoveIngredient: (ingredient) => dispatch(removeIngredient(ingredient)),
  onInitIngredients: () => dispatch(initIngredients()),
  onInitPurchase: () => dispatch(purchaseInit()),
  onSetAuthRedirectPath: (path) => dispatch(setAuthRedirectPath(path))
})


export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurguerBuilder, axios))
