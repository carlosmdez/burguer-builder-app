import React from 'react'
import { BurguerBuilder } from './BurguerBuilder'
import BuildControls from '../../components/Burguer/BuildControls/BuildControls';
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()})

describe('<BurgerBuilder/>', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<BurguerBuilder onInitIngredients={() => {}}/>)
  })

  it('should render <BuildControls> when receiving ingredients', () => {
    wrapper.setProps({ingBruguer: {salad: 0}})
    expect(wrapper.find(BuildControls)).toHaveLength(1)
  })
})