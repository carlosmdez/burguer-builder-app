import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import CheckoutSumarry from '../../components/Order/CheckoutSumarry/CheckoutSumarry'
import ContactData from './ContactData/ContactData'


class Checkout extends Component {

  chechoutCancelledHandler = () => {
    this.props.history.goBack()
  }

  chechoutContinuedHandler = () => {
    this.props.history.replace('/checkout/contact')
  }

  render() {
    let sumarry = <Redirect to='/'/>
    if (this.props.ingredients) {
      const purcharseRedirect = this.props.purchased ? <Redirect to='/'/> : null
      sumarry = (
        <div>
          {purcharseRedirect}
        <CheckoutSumarry
          ingredients={this.props.ingredients}
          chechoutCancelled={this.chechoutCancelledHandler}
          chechoutContinued={this.chechoutContinuedHandler}
        />
        <Route
          path={`${this.props.match.path}/contact`}
          component={ContactData}
        />
      </div>
      )
    }
    return sumarry
  }
}

const mapStateToProps = state => ({
  ingredients: state.reducerBurguer.ingredients,
  purchased: state.orderReducer.purchased
})


export default connect(mapStateToProps)(Checkout)
