import React, { Component } from 'react'
import { connect } from 'react-redux'

import Button from '../../../components/UI/Button/Button'
import css from './ContactData.css'
import axios from '../../../axios-orders'
import Input from '../../../components/UI/Input/Input'
import withErrorHandler from '../../../components/withErrorHandler/withErrorHandler'
import { purchaseBurguer } from '../../../store/actions/actions';

class ContactData extends Component {
  state = {
    orderForm: {
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Name'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your e-mail'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      street: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Street'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      zipCode: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Zip Code'
        },
        value: '',
        validation: {
          required: true,
          minLength: 5,
          maxLength: 5
        },
        valid: false,
        touched: false
      },
      country: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Country'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      deliveryMethod: {
        elementType: 'select',
        elementConfig: {
          options: [
            { value: 'fastest', displayValue: 'Fastest' },
            { value: 'normal', displayValue: 'Normal' },
            { value: 'cheapest', displayValue: 'Cheapest' }
          ]
        },
        value: 'fastest',
        valid: true
      }
    },
    formIsValid: false
  }

  orderHandler = event => {
    event.preventDefault()
    const formData = {}
    for (let key in this.state.orderForm) {
      formData[key] = this.state.orderForm[key].value
    }
    const order = {
      userId: this.props.userId,
      ingredients: this.props.ingBruguer,
      price: this.props.priceBurguer,
      orderData: formData
    }
    this.props.onOrderBurguer(order, this.props.token)
  }

  checkValidity = (value, rules) => {
    let isValid = true
    if (rules) {
      if (rules.required) {
        isValid = value.trim() !== '' && isValid
      }
  
      if (rules.minLength) {
        isValid = value.length >= rules.minLength && isValid
      }
  
      if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid
      }
    }
    return isValid
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {...this.state.orderForm}
    const updatedFormElement = {...updatedOrderForm[inputIdentifier]}
    updatedFormElement.value = event.target.value
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation)
    updatedFormElement.touched = true
    updatedOrderForm[inputIdentifier] = updatedFormElement
    
    let formIsValid = true
    for (const key in updatedOrderForm) {
      formIsValid = updatedOrderForm[key].valid && formIsValid
    }

    this.setState({orderForm : updatedOrderForm, formIsValid})
  }

  render() {
    const formElementsArray = Object.entries(this.state.orderForm)
    let form = (
      <form onSubmit={this.orderHandler}>
        {formElementsArray.map(formElement => (
          <Input
            key={formElement[0]}
            elementType={formElement[1].elementType}
            elementConfig={formElement[1].elementConfig}
            value={formElement[1].value}
            invalid={!formElement[1].valid}
            shouldValidate = {formElement[1].validation}
            touched = {formElement[1].touched}
            changed={(event) => this.inputChangedHandler(event, formElement[0])}
          />
        ))}
        <Button disabled={!this.state.formIsValid} buttonType="Success">
          Order
        </Button>
      </form>
    )
    // if (this.props.loading) {
    //   form = <Spinner />
    // }
    return (
      <div className={css.ContactData}>
        <h3>Enter your Contact Data </h3>
        {form}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  ingBruguer: state.reducerBurguer.ingredients,
  priceBurguer: state.reducerBurguer.totalPrice,
  loading: state.orderReducer.loading,
  token:state.authReducer.token,
  userId: state.authReducer.userId
})

const mapDispatchToProps = dispatch => ({
  onOrderBurguer: (order, token) => dispatch(purchaseBurguer(order, token))
})

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios))